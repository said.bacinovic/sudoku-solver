#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>

#define HEIGHT 9
#define WIDTH 9

bool checkEnd(int[][WIDTH]);
bool checkColumn(int[][WIDTH], int, int);
bool checkRow(int[][WIDTH], int, int);
bool checkBox(int[][WIDTH], int, int);
bool checkEmpty(int[][WIDTH]);
bool checkValid(int[][WIDTH], int i, int j);
void printMatrix(int [][WIDTH]);
void prettyPrint(int [][WIDTH]);

void eraseLines();

void solve(int[][WIDTH],int[][WIDTH], int );


int main()
{
  int matrix [HEIGHT][WIDTH];
  int startingMatrix[HEIGHT][WIDTH];

  std::cout<<"Enter sudoku puzzle: "<<std::endl;
  int x;
  for(int i=0; i<HEIGHT; i++)
    for(int j=0; j<WIDTH; j++)
    {
      std::cin>>x;
      matrix[i][j]=x;
      startingMatrix[i][j]=x;
    }
 

  solve(matrix, startingMatrix, 0);
  if(!checkEnd(matrix))
    std::cout<<"There is no possible solution for this puzzle."<<std::endl;
  return 0;
}

void solve(int matrix[][WIDTH], int startingMatrix [][WIDTH], int position)
{
  int i=position/9;
  int j=position%9;
  
  if(startingMatrix[i][j]!=0)
  {
    solve(matrix, startingMatrix, position+1);
    return;
  }

  for(int x=1; x<=9; ++x)
  {
    matrix[i][j]=x;

    //eraseLines();         //include this two lines if you want to see solution being worked out in
    //printMatrix(matrix);  //slow-motion in linux terminal.. also you can change speed in eraseLines function

    bool valid=checkValid(matrix, i, j);
    if(valid) 
    {
      if(checkEnd(matrix))
      {
        std::cout<<"SOLUTION:"<<std::endl;
        prettyPrint(matrix);
        return;
      }
    }
    if(checkEnd(matrix)) return;
    if(valid)
    {
      solve(matrix, startingMatrix, position+1);
    }
    if(checkEnd(matrix)) return;
  }
  matrix[i][j]=0;
}

void eraseLines()
{
  std::cout<<"\033c";
  usleep(10);
}

void prettyPrint(int matrix[][WIDTH])
{
  for(int i=0; i<HEIGHT; i++)
  {
    if(i%3==0) std::cout<<"  -------------------------"<<std::endl;
    for(int j=0; j<WIDTH; j++)
    {
      if(j%3==0) std::cout<<" | ";
      std::cout<<matrix[i][j]<<" ";

    }
    std::cout<<"|"<<std::endl;
  }
  
  std::cout<<"  -------------------------"<<std::endl;
}

void printMatrix(int matrix [][WIDTH])
{
  for(int i=0; i<HEIGHT; i++)
  {
    for(int j=0; j<WIDTH; j++)
    {
      std::cout<<matrix[i][j]<<" ";
    }
    std::cout<<std::endl;
  }
}

bool checkEnd(int matrix [][WIDTH] )
{
  bool finished = true;
  if(checkEmpty(matrix)) return false;
  for(int i=0; i<HEIGHT; i++)
  {
    for(int j=0; j<WIDTH; j++)
    {
      bool col=checkColumn(matrix, i, j);
      bool row=checkRow(matrix, i, j);
      bool box=checkBox(matrix, i, j);
      if(col || row || box)
      {
        finished = false;
        break;
      }
    }
    
    if(finished == false) break;
  }
  return finished;
}

bool checkColumn(int matrix [][WIDTH], int i, int j)
{
  int curValue=matrix[i][j];
  for(int x=0; x<HEIGHT; x++)
  {
    if(curValue==matrix[x][j] && x!=i)
    {
      return true;
    }
  }
  return false;
}

bool checkRow(int matrix [][WIDTH], int i, int j)
{
  int curValue=matrix[i][j];
  for(int x=0; x<WIDTH; x++)
  {
    if(curValue==matrix[i][x] && x!=j)
    {
      return true;
    }
  }
  return false;
}

bool checkBox(int matrix [][WIDTH], int i, int j)
{
  int curValue=matrix[i][j];

  int boxI=i/3;
  int boxJ=j/3;

  for(int x=3*boxI; x<3*boxI+3; x++)
    for(int y=3*boxJ; y<3*boxJ+3; y++)
    {
      if(matrix[x][y]==curValue && (x!=i || y!=j))
        return true;
    }

  return false;
}

bool checkEmpty(int matrix[][WIDTH])
{
  for(int i=0; i<HEIGHT; i++)
  {
    for(int j=0; j<WIDTH; j++)
    {
      if(matrix[i][j]==0)
        return true;
    }
  }
  return false;
}

bool checkValid(int matrix[][WIDTH], int i, int j)
{
  if(checkColumn(matrix, i, j)) return false;
  if(checkRow(matrix, i, j)) return false;
  if(checkBox(matrix, i, j)) return false;
  return true;
}

